<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('login');
});

Route::get('/accessdenied', 'HomeController@denied')->name('access.denied');
Route::get('getpart/{id}', 'BarangController@getPart');
Route::get('getmerek/{merek}', 'BarangController@getMerek');
Route::post('login','AuthController@login')->name('login');

Route::group(['prefix' => 'barang'], function () {
    Route::get('/' ,['as' => 'barang' ,'uses' => 'BarangController@index']);
    Route::get('/create', ['as' => 'barang.create', 'uses' => 'BarangController@create']);
    Route::post('/store', ['as' => 'barang.store', 'uses' => 'BarangController@store']);
    Route::get('/delete/{id}',['as' => 'barang.delete', 'uses' => 'BarangController@delete']);
    Route::get('/edit/{id}',['as' => 'barang.edit', 'uses' => 'BarangController@edit']);
    Route::PATCH('/update/{id}', ['as' => 'barang.update' , 'uses' => 'BarangController@update']);
    // Route::post('/create',['as' => 'barang.create','uses' => 'BarangController@create']);

});

Route::group(['middleware' => ['Login']], function () {
    Route::group(['prefix' => 'barang'], function () {
        Route::get('/' ,['as' => 'barang' ,'uses' => 'BarangController@index']);
        Route::get('/create', ['as' => 'barang.create', 'uses' => 'BarangController@create']);
        Route::post('/store', ['as' => 'barang.store', 'uses' => 'BarangController@store']);
        Route::get('/edit/{id}',['as' => 'barang.edit', 'uses' => 'BarangController@edit']);
        Route::PATCH('/update/{id}', ['as' => 'barang.update' , 'uses' => 'BarangController@update']);
        Route::get('/choose/{id}',['as' => 'barang.choose', 'uses' => 'BarangController@choose']);
        Route::get('/delete/{id}',['as' => 'barang.delete', 'uses' => 'BarangController@delete']);
    });
});

Route::group(['middleware' => ['Supplier']], function () {

    Route::group(['prefix' => 'supplier'], function () {
        Route::get('/' ,['as' => 'supplier' ,'uses' => 'SupplierController@index']);
        Route::get('/create', ['as' => 'supplier.create', 'uses' => 'SupplierController@create']);
        Route::post('/store', ['as' => 'supplier.store', 'uses' => 'SupplierController@store']);
        Route::get('/delete/{id}',['as' => 'supplier.delete', 'uses' => 'SupplierController@delete']);
        Route::get('/edit/{id}',['as' => 'supplier.edit', 'uses' => 'SupplierController@edit']);
        Route::PATCH('/update/{id}', ['as' => 'supplier.update' , 'uses' => 'SupplierController@update']);
    });

});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/alterpart', 'HomeController@create')->name('alterpart.create');
Route::post('/store', 'HomeController@store')->name('alterpart.store');
Route::get('/delete/{id}', 'HomeController@delete')->name('alterpart.delete');
Route::get('/pilih/{id}', 'HomeController@pilih')->name('alterpart.pilih');
