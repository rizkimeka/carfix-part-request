@extends('template.main')

@section('title','Report Part Data')

@section('content')
{{-- @if(auth::user()->hak_akses == 1)
<div class="container">
    <div class="row">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Hi Partman!</div>
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                </div>
            </div>
        </div>
    </div>
</div>
@endif
@if(auth::user()->hak_akses == 2)
<div class="container">
    <div class="row">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Hi Supplier!</div>
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                </div>
            </div>
        </div>
    </div>
</div>
@endif --}}
<div class="row">
        <div class="col-lg-3 col-md-6 col-sm-6 col-12">
            <div class="card card-statistic-1">
                <div class="card-icon bg-transparent">
                    <i class="fas fa-cubes"></i>
                </div>
                <div class="card-wrap">
                    <div class="card-header">
                        <a href="{{ route('barang') }}">
                            <h4></h4>
                        </a>
                    </div>
                    <div class="card-body">
                        {{-- @php
                        $barang = DB::table('tb_barang')->count();
                        @endphp
                        {{ $barang }} --}}
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6 col-12">
            <div class="card card-statistic-1">
                <div class="card-icon bg-warning">
                    <i class="fas fa-fw fa-cubes"></i>
                </div>
                <div class="card-wrap">
                    <div class="card-header">
                        <a href="{{ route('barang') }}">
                            <h4>Total Barang</h4>
                        </a>
                    </div>
                    <div class="card-body">
                        @php
                        $barang = DB::table('tb_barang')->count();
                        @endphp
                        {{ $barang }}
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6 col-12">
            <div class="card card-statistic-1">
                <div class="card-icon bg-success">
                    <i class="fas fa-fw fa-cart-plus"></i>
                </div>
                <div class="card-wrap">
                    <div class="card-header">
                        <a href="{{ route('supplier') }}">
                            <h4>Total Supplier</h4>
                        </a>
                    </div>
                    <div class="card-body">
                        @php
                        $supplier = DB::table('tb_supplier')->count();
                        @endphp
                        {{ $supplier }}
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6 col-12">
            <div class="card card-statistic-1">
                <div class="card-icon bg-transparent">
                    <i class="fas fa-fw fa-cart-plus"></i>
                </div>
                <div class="card-wrap">
                    <div class="card-header">
                        <a href="{{ route('supplier') }}">
                            <h4></h4>
                        </a>
                    </div>
                    <div class="card-body">
                        {{-- @php
                        $supplier = DB::table('tb_supplier')->count();
                        @endphp
                        {{ $supplier }} --}}
                    </div>
                </div>
            </div>
        </div>
    </div>
@if(auth::user()->hak_akses == 1)
@include('template.alert')
    <div class="card shadow">
      <div class="card-header">
        <h4>Report From Supplier Respon</h4>
        <div class="card-header-action">
          {{-- <a href="{{ route('barang.create') }}" class="btn btn-info">Add Data <i class="fas fa-plus"></i></a> --}}
        </div>
      </div>
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-bordered table-striped table-hover" id="example1">
            <thead>
              <tr>
                <th><i class="fas fa-th"></i></th>
                <th>Kode Part</th>
                <th>Brand Part</th>
                <th>Merek Mobil</th>
                <th>Model Mobil</th>
                <th>Tahun</th>
                <th>Expected ETA</th>
                <th>Nama Part</th>
                <th>Quantity</th>
                <th>Status</th>
                {{-- <th>Tanggal Input</th>
                <th>Harga</th>
                <th>Supplier</th> --}}
                {{-- <th>Action</th> --}}
              </tr>
            </thead>
            <tbody>
            @if(count($data) > 0)
                @foreach($data as $field)
                <tr>
                  <td>{{ $loop->iteration }}</td>
                  <td>{{ $field->kode_part }}</td>
                  <td>{{ $field->brand }}</td>
                  <td>{{ $field->merek }}</td>
                  <td>{{ $field->model }}</td>
                  <td>{{ $field->tahun }}</td>
                  <td>{{ $field->eta }}</td>
                  <td nowrap="">{{ $field->nama_part }}</td>
                  <td >{{ $field->quantity }}</td>
                  <td>
                        @if($field->status == 1)
                        <span class="badge badge-info">Sudah Direspon</span>
                        @elseif($field->status == 2)
                        <span class="badge badge-danger">Belum Direspon/Tidak Tersedia</span>
                        @elseif($field->status == 3)
                        <span class="badge badge-success">Sudah Dipilih</span>
                        @endif
                  </td>
                  {{-- <td>{{ $field->tanggal }}</td>
                  <td>{{ $field->harga }}</td>
                  <td>{{ $field->supplier }}</td> --}}
                  {{-- <td>
                    <a href="{{ route('barang.edit', [$field->kode_part]) }}" class="btn btn-icon btn-primary"><i class="fas fa-pen"></i></a>
                    <a onclick="return confirm('Apa anda yakin?')" href="{{ route('barang.delete', [$field->kode_part]) }}" class="btn btn-danger"><i class="menu-icon fa fa-trash"></i>
                    </a>
                  </td> --}}
                </tr>
                @endforeach
              @else
                <tr class="text-center">
                  <td colspan="4">No data found</td>
                </tr>
              @endif
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <div class="card shadow">
      <div class="card-header">
        <h4>Alternative Part</h4>
        <div class="card-header-action">
          {{-- <a href="{{ route('barang.create') }}" class="btn btn-info">Add Data <i class="fas fa-plus"></i></a> --}}
        </div>
      </div>
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-bordered table-striped table-hover" id="example">
            <thead>
              <tr>
                <th><i class="fas fa-th"></i></th>
                <th>Kode Part Request</th>
                <th>Kode Part Alternative</th>
                <th>Part Alternative</th>
                <th>Brand Part</th>
                <th>Merek Mobil</th>
                <th>Model Mobil</th>
                <th>Tahun</th>
                <th>Quantity</th>
                <th>Harga</th>
                <th>Status</th>
                {{-- <th>Tanggal Input</th>
                <th>Harga</th>
                <th>Supplier</th> --}}
                {{-- <th>Action</th> --}}
              </tr>
            </thead>
            <tbody>
            @if(count($datas) > 0)
                @foreach($datas as $field)
                <tr>
                  <td>{{ $loop->iteration }}</td>
                  <td>{{ $field->part->kode_part }}</td>
                  <td>{{ $field->kode_alterpart }}</td>
                  <td nowrap="">{{ $field->nama_part }}</td>
                  <td>{{ $field->brand }}</td>
                  <td>{{ $field->merek }}</td>
                  <td>{{ $field->model }}</td>
                  <td>{{ $field->tahun }}</td>
                  <td>{{ $field->quantity }}</td>
                  <td>{{ $field->harga }}</td>
                  <td>
                        @if($field->status == 1)
                        <span class="badge badge-info">Sudah Direspon</span>
                        @elseif($field->status == 2)
                        <span class="badge badge-danger">Belum Direspon</span>
                        @elseif($field->status == 3)
                        <span class="badge badge-success">Sudah Dipilih</span>
                        @endif
                  </td>
                </tr>
                @endforeach
              @else
                <tr class="text-center">
                  <td colspan="4">No data found</td>
                </tr>
              @endif
            </tbody>
          </table>
        </div>
      </div>
    </div>
@endif
@endsection