@extends('template.main')

@section('title','Input Part')

@section('content')
<div class="section-body">
    <h2 class="section-title">Barang</h2>
    <p class="section-lead">Halaman index barang</p>
    @if(Auth::user()->hak_akses == 1)
    @include('template.alert')
    <div class="card shadow">
      <div class="card-header">
        <h4>Data Table</h4>
        <div class="card-header-action">
          <a href="{{ route('barang.create') }}" class="btn btn-info">Add Data <i class="fas fa-plus"></i></a>
        </div>
      </div>
      <div class="card-body">
        <div class="table-responsive table-invoice">
          <table class="table table-bordered table-striped table-hover" id="example">
            <thead>
              <tr>
                <th><i class="fas fa-th"></i></th>
                {{-- <th>Supplier</th> --}}
                <th>Kode Part</th>
                <th>Nama Part</th>
                <th>Brand Part</th>
                <th>Merek Mobil</th>
                <th>Model Mobil</th>
                <th>Tahun</th>
                <th>Quantity</th>
                <th>Expected ETA</th>
                <th>Harga</th>
                {{-- <th>UoM</th> --}}
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
            @if(count($data) > 0)
                @foreach($data as $field)
                <tr>
                  <td>{{ $loop->iteration }}</td>
                  {{-- <td>{{ $field->supplier->nama_supplier }}</td> --}}
                  <td>{{ $field->kode_part }}</td>
                  <td nowrap="">{{ $field->nama_part }}</td>
                  <td>{{ $field->brand }}</td>
                  <td>{{ $field->merek }}</td>
                  <td>{{ $field->model }}</td>
                  <td>{{ $field->tahun }}</td>
                  <td>{{ $field->quantity }}</td>
                  <td>{{ $field->eta }}</td>
                  <td>{{ $field->harga }}</td>
                  <td nowrap="">
                    <a href="{{ route('barang.pilih', [$field->kode_part]) }}" class="btn btn-icon btn-success"><i class="fas fa-check"></i></a>
                    <a onclick="return confirm('Apa anda yakin?')" href="{{ route('barang.delete', [$field->kode_part]) }}" class="btn btn-danger"><i class="menu-icon fa fa-trash"></i>
                    </a>
                  </td>
                </tr>
                @endforeach
              @else
                <tr class="text-center">
                  <td colspan="4">No data found</td>
                </tr>
              @endif
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <div class="card shadow">
      <div class="card-header">
        <h4>Alternative Part</h4>
        <div class="card-header-action">
          {{-- <a href="{{ route('barang.create') }}" class="btn btn-info">Add Data <i class="fas fa-plus"></i></a> --}}
        </div>
      </div>
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-bordered table-striped table-hover" id="example">
            <thead>
              <tr>
                <th><i class="fas fa-th"></i></th>
                <th>Kode Part Request</th>
                <th>Kode Part Alternative</th>
                <th>Part Alternative</th>
                <th>Brand Part</th>
                <th>Merek Mobil</th>
                <th>Model Mobil</th>
                <th>Tahun</th>
                <th>Quantity</th>
                <th>Harga</th>
                <!--<th>Status</th>-->
                <!--<th>Harga</th>-->
                {{-- <th>Tanggal Input</th>
                <th>Supplier</th> --}}
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
            @if(count($datas) > 0)
                @foreach($datas as $field)
                <tr>
                  <td>{{ $loop->iteration }}</td>
                  <td>{{ $field->kode_part }}</td>
                  <td>{{ $field->kode_alterpart }}</td>
                  <td nowrap="">{{ $field->nama_part }}</td>
                  <td>{{ $field->brand }}</td>
                  <td>{{ $field->merek }}</td>
                  <td>{{ $field->model }}</td>
                  <td>{{ $field->tahun }}</td>
                  <td>{{ $field->quantity }}</td>
                  <td>{{ $field->harga }}</td>
                  <td nowrap="">
                    <a href="{{ route('alterpart.pilih', [$field->kode_part]) }}" class="btn btn-icon btn-success"><i class="fas fa-check"></i></a>
                    <a onclick="return confirm('Apa anda yakin?')" href="{{ route('alterpart.delete', [$field->kode_part]) }}" class="btn btn-danger"><i class="menu-icon fa fa-trash"></i>
                    </a>
                  </td>
                  <!--<td>-->
                  <!--      @if($field->status == 1)-->
                  <!--      <span class="badge badge-info">Sudah Direspon</span>-->
                  <!--      @elseif($field->status == 2)-->
                  <!--      <span class="badge badge-danger">Belum Direspon</span>-->
                  <!--      @elseif($field->status == 3)-->
                  <!--      <span class="badge badge-success">Sudah Dipilih</span>-->
                  <!--      @endif-->
                  <!--</td>-->
                  <!--<td>{{ $field->harga }}</td>-->
                </tr>
                @endforeach
              @else
                <tr class="text-center">
                  <td colspan="4">No data found</td>
                </tr>
              @endif
            </tbody>
          </table>
        </div>
      </div>
    </div>
@endif
@if(Auth::user()->hak_akses == 2)
@include('template.alert')
    <div class="card shadow">
      <div class="card-header">
        <h4>Data Table</h4>
        <div class="card-header-action">
          <a href="{{ route('barang.create') }}" class="btn btn-info">Add Data <i class="fas fa-plus"></i></a>
        </div>
      </div>
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-bordered table-striped table-hover" id="example">
            <thead>
              <tr>
                <th>No. Masuk</th>
                <th>Kode Part</th>
                <th>Brand Part</th>
                <th>Merek Mobil</th>
                <th>Model Mobil</th>
                <th>Tahun</th>
                <th>Expected ETA</th>
                <th>Nama Part</th>
                <th>Quantity</th>
                <th>Tanggal Input</th>
                <th>Harga</th>
                <th>Supplier</th>
                <th width="20%">Action</th>
              </tr>
            </thead>
            <tbody>
            @if(count($data) > 0)
                @foreach($data as $field)
                @if($field->supplier == auth::user()->name)
                <tr>
                  <td>{{ $loop->iteration }}</td>
                  <td>{{ $field->kode_part }}</td>
                  <td>{{ $field->brand }}</td>
                  <td nowrap="">{{ $field->merek }}</td>
                  <td>{{ $field->model }}</td>
                  <td>{{ $field->tahun }}</td>
                  <td>{{ $field->eta }}</td>
                  <td nowrap="">{{ $field->nama_part }}</td>
                  <td>{{ $field->quantity }}</td>
                  <td>{{ $field->tanggal }}</td>
                  <td>{{ $field->harga }}</td>
                  <td>{{ auth::user()->name }}</td>
                  <td nowrap="">
                    <a href="{{ route('barang.edit', [$field->kode_part]) }}" class="btn btn-icon btn-primary"><i class="fas fa-pen"></i></a>
                    <a onclick="return confirm('Apa anda yakin?')" href="{{ route('barang.delete', [$field->kode_part]) }}" class="btn btn-danger"><i class="menu-icon fa fa-trash"></i>
                    </a>
                  </td>
                </tr>
                @endif
                @endforeach
              @else
                <tr class="text-center">
                  <td colspan="4">No data found</td>
                </tr>
            @endif
            </tbody>
          </table>
        </div>
      </div>
    </div>
@endif
  </div>
@endsection