@extends('template.main')

@section('title','Edit Part')

@section('content')
<div class="section-body">
    <h2 class="section-title">Edit Part</h2>
    <p class="section-lead">Halaman untuk edit part barang</p>
    @if(Auth::user()->hak_akses == 1)
    @include('template.alert')
    <div class="card">
        <form action="{{ route('barang.update',$data['kode_part']) }}" method="post">
            @csrf @method('patch')
        <div class="card-header">
          <h4>Part Data <a href="{{ route('barang') }}" class="btn btn-info ml-2"><i class="fas fa-arrow-left"></i> Back</a></h4>
        </div>
        <div class="card-body">
          {{-- <div class="form-group row">
            <label class="col-12 col-md-2 col-form-label text-md-right">Supplier</label>
            <div class="col-sm-12 col-md-8">
                <select name="kode_supplier" class="form-control" disabled="">
                    @foreach($datas as $field)
                        @if($field->kode_supplier == $data->kode_supplier)
                        <option value="{{ $field->kode_supplier }}" selected>{{ $field->nama_supplier }}</option>
                        @else
                        <option value="{{ $field->kode_supplier }}">{{ $field->nama_supplier }}</option>
                        @endif
                    @endforeach
                </select>
            </div>
          </div> --}}

          <div class="form-group row">
            <label class="col-12 col-md-2 col-form-label text-md-right">Kode Part</label>
            <div class="col-sm-12 col-md-8">
              <input type="text" class="form-control" name="kode_part" disabled="" value="{{ $data->kode_part}}">
            </div>
          </div>

          <div class="form-group row">
            <label class="col-12 col-md-2 col-form-label text-md-right">Brand Part</label>
            <div class="col-sm-12 col-md-8">
              <input type="text" class="form-control" name="brand" value="{{ $data->brand}}">
            </div>
          </div>

          <div class="form-group row">
            <label class="col-12 col-md-2 col-form-label text-md-right">Merek Mobil</label>
            <div class="col-sm-12 col-md-8">
                <select name="merek" id="merek" class="form-control select2" required="" onchange="ada()">
                    <option value=""></option>
                    @foreach ($mobil as $e)
                    <option value="{{$e->merek}}" {{ $e->merek == $data->merek ? 'selected':'' }}>{{$e->merek}}</option>
                    @endforeach
                </select>
            </div>
          </div>

          <div class="form-group row">
            <label class="col-12 col-md-2 col-form-label text-md-right">Model Mobil</label>
            <div class="col-sm-12 col-md-8">
                <select name="model" id="model" class="form-control select2" required="">
                    <option value="" selected></option>
                    @foreach ($model as $e)
                    <option value="{{$e->model}}" {{ $e->model == $data->model ? 'selected':'' }}>{{$e->model}}</option>
                    @endforeach
                </select>
            </div>
          </div>

          <div class="form-group row">
            <label class="col-12 col-md-2 col-form-label text-md-right">Nama Part</label>
            <div class="col-sm-12 col-md-8">
                <input type="text" class="form-control" name="nama_part" required="" value="{{ $data->nama_part}}">
            </div>
          </div>

          <div class="form-group row">
                <label class="col-12 col-md-2 col-form-label text-md-right">Tahun</label>
                <div class="col-sm-12 col-md-8">
                <input type="number" class="form-control" id="yearpicker" placeholder="YYYY" min="1950" max="2020" maxlength="4" name="tahun" required="" value="{{ $data->tahun }}">
                </div>
          </div>

          <div class="form-group row">
            <label class="col-12 col-md-2 col-form-label text-md-right">Quantity</label>
            <div class="col-sm-12 col-md-8">
                <input type="text" class="form-control" name="quantity" required="" value="{{ $data->quantity}}">
            </div>
          </div>

          <div class="form-group row">
            <label class="col-12 col-md-2 col-form-label text-md-right">Expected ETA</label>
            <div class="col-sm-12 col-md-8">
                <input type="date" class="form-control" name="eta" required="" value="{{ $data->eta }}">
            </div>
          </div>

          {{-- <div class="form-group row">
            <label class="col-12 col-md-2 col-form-label text-md-right">UoM</label>
            <div class="col-sm-12 col-md-8">
                <select name="uom" class="form-control" required="">
                    <option value="">Pilih UoM</option>
                    <option value="PCS" {{ $data->uom == 'PCS' ? 'selected':'' }}>PCS</option>
                    <option value="Liter" {{ $data->uom == 'Liter' ? 'selected':'' }}>Liter</option>
                    <option value="Box" {{ $data->uom == 'Box' ? 'selected':'' }}>Box</option>
                    <option value="Drum" {{ $data->uom == 'Drum' ? 'selected':'' }}>Drum</option>
                    <option value="Galon" {{ $data->uom == 'Galon' ? 'selected':'' }}>Galon</option>
                    <option value="Lusin" {{ $data->uom == 'Lusin' ? 'selected':'' }}>Lusin</option>
                    <option value="Kg" {{ $data->uom == 'Kg' ? 'selected':'' }}>Kg</option>
                  </select>
            </div>
          </div>

          <small id="formatUang" style="text-align:center;" class="form-text text-muted"></small>
          <div class="form-group row">
            <label class="col-12 col-md-2 col-form-label text-md-right">Harga</label>
            <div class="col-sm-12 col-md-8">
                <div class="input-group-prepend">
                    <span class="input-group-text">Rp.</span>
                <input type="number" class="form-control" name="harga" required="" onkeyup="formatRupiah(this.value,'formatUang')" value="{{ $data->harga }}">
                </div>
            </div>
          </div> --}}

        </div>
        <div class="card-footer text-right">
            <a href="{{ route('barang') }}" class="btn btn-danger ml-2">Cancel</a>
            <button class="btn btn-primary">Submit</button>
        </div>
      </form>
    </div>
@endif
@if(Auth::user()->hak_akses == 2)
    @include('template.alert')
    <div class="card">
        <form action="{{ route('barang.update',$data['kode_part']) }}" method="post">
            @csrf @method('patch')
        <div class="card-header">
          <h4>Part Data <a href="{{ route('barang') }}" class="btn btn-info ml-2"><i class="fas fa-arrow-left"></i> Back</a></h4>
        </div>
        <div class="card-body">
          <div class="form-group row">
            <label class="col-12 col-md-2 col-form-label text-md-right">Supplier</label>
            <div class="col-sm-12 col-md-8">
                <input type="text" class="form-control" name="kode_supplier" disabled="" value="{{ Auth::user()->name }}">
            </div>
          </div>

          <div class="form-group row">
            <label class="col-12 col-md-2 col-form-label text-md-right">Kode Part</label>
            <div class="col-sm-12 col-md-8">
              <input type="text" class="form-control" name="kode_part" disabled="" value="{{ $data->kode_part}}">
            </div>
          </div>

          <div class="form-group row">
            <label class="col-12 col-md-2 col-form-label text-md-right">Brand Part</label>
            <div class="col-sm-12 col-md-8">
              <input type="text" class="form-control" name="brand" value="{{ $data->brand}}">
            </div>
          </div>

          <div class="form-group row">
            <label class="col-12 col-md-2 col-form-label text-md-right">Merek Mobil</label>
            <div class="col-sm-12 col-md-8">
                <select name="merek" id="merek" class="form-control select2" required="" onchange="ada()">
                    <option value=""></option>
                    @foreach ($mobil as $e)
                    <option value="{{$e->merek}}" {{ $e->merek == $data->merek ? 'selected':'' }}>{{$e->merek}}</option>
                    @endforeach
                </select>
            </div>
          </div>

          <div class="form-group row">
                <label class="col-12 col-md-2 col-form-label text-md-right">Model Mobil</label>
                <div class="col-sm-12 col-md-8">
                    <select name="model" id="model" class="form-control select2" required="">
                        <option value=""></option>
                        @foreach ($model as $e)
                        <option value="{{$e->model}}" {{ $e->model == $data->model ? 'selected':'' }}>{{$e->model}}</option>
                        @endforeach
                    </select>
                </div>
        </div>

        <div class="form-group row">
                <label class="col-12 col-md-2 col-form-label text-md-right">Tahun</label>
                <div class="col-sm-12 col-md-8" id="tahun">
                <input type="number" class="form-control" id="yearpicker" placeholder="YYYY" min="1950" max="2020" maxlength="4" name="tahun" required="" value="{{ $data->tahun }}">
                </div>
          </div>

          <div class="form-group row">
            <label class="col-12 col-md-2 col-form-label text-md-right">Expected ETA</label>
            <div class="col-sm-12 col-md-8">
            <input type="date" class="form-control" name="eta" required="" value="{{ $data->eta }}">
            </div>
          </div>

          <div class="form-group row">
            <label class="col-12 col-md-2 col-form-label text-md-right">Nama Part</label>
            <div class="col-sm-12 col-md-8">
                <input type="text" class="form-control" name="nama_part" required="" value="{{ $data->nama_part}}">
            </div>
          </div>

          <div class="form-group row">
            <label class="col-12 col-md-2 col-form-label text-md-right">Quantity</label>
            <div class="col-sm-12 col-md-8">
                <input type="text" class="form-control" name="quantity" required="" value="{{ $data->quantity}}">
                    {{-- @php
                        $tanggal = date("Y-m-d");
                    @endphp
                <input type="hidden" class="form-control" name="tanggal" required="" value="{{ $tanggal }}"> --}}
            </div>
          </div>

          {{-- <div class="form-group row">
            <label class="col-12 col-md-2 col-form-label text-md-right">UoM</label>
            <div class="col-sm-12 col-md-8">
                <select name="uom" class="form-control" required="">
                    <option value="">Pilih UoM</option>
                    <option value="PCS" {{ $data->uom == 'PCS' ? 'selected':'' }}>PCS</option>
                    <option value="Liter" {{ $data->uom == 'Liter' ? 'selected':'' }}>Liter</option>
                    <option value="Box" {{ $data->uom == 'Box' ? 'selected':'' }}>Box</option>
                    <option value="Drum" {{ $data->uom == 'Drum' ? 'selected':'' }}>Drum</option>
                    <option value="Galon" {{ $data->uom == 'Galon' ? 'selected':'' }}>Galon</option>
                    <option value="Lusin" {{ $data->uom == 'Lusin' ? 'selected':'' }}>Lusin</option>
                    <option value="Kg" {{ $data->uom == 'Kg' ? 'selected':'' }}>Kg</option>
                  </select>
            </div>
          </div> --}}

          <small id="formatUang" style="text-align:center;" class="form-text text-muted"></small>
          <div class="form-group row">
            <label class="col-12 col-md-2 col-form-label text-md-right">Harga</label>
            <div class="col-sm-12 col-md-8">
                <div class="input-group-prepend">
                    <span class="input-group-text">Rp.</span>
                <input type="number" class="form-control" name="harga" required="" onkeyup="formatRupiah(this.value,'formatUang')" value="{{ $data->harga }}">
                </div>
            </div>
          </div>

        </div>
        <div class="card-footer text-right">
            <a href="{{ route('barang') }}" class="btn btn-danger ml-2">Cancel</a>
            <button class="btn btn-primary">Submit</button>
        </div>
      </form>
    </div>
@endif
  </div>

  <script>
      function formatRupiah(angka, target) {
            var number_string = angka.replace(/[^,\d]/g, '').toString(),
                split = number_string.split(','),
                sisa = split[0].length % 3,
                rupiah = split[0].substr(0, sisa),
                ribuan = split[0].substr(sisa).match(/\d{3}/gi);

            // tambahkan titik jika yang di input sudah menjadi angka ribuan
            if (ribuan) {
                separator = sisa ? '.' : '';
                rupiah += separator + ribuan.join('.');
            }

            rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
            document.getElementById(target).innerHTML = 'Rp. ' + rupiah;
        }
  </script>
  <script>
  function ada() {
          var cek = $('#merek').val();
          console.log(cek)
          var selectMobil = $('#model');

          $.ajax({
            url: "{{ url('getmerek')}}" + "/" + cek,
            type: "GET",
            dataType: "json",
            success: function(data) {
                // console.log(data)
                selectMobil.empty()
                1 // $('select[name="id_Mobil"]').empty('');
                selectMobil.append('<option value=""></option>');
                $.each(data, function(key, value) {
                    selectMobil.append('<option value="' + value.model + '">' + value.model + '</option>');
                    // $('select[name="id_kota"]').attr('disabled',false);
                });
            }
        });
    }
    </script>
@endsection