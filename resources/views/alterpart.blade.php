@extends('template.main')

@section('title','Alternative Part')

@section('content')
<div class="section-body">
    <h2 class="section-title">Alternative Part</h2>
    <p class="section-lead">Halaman untuk input alternative part</p>
@if(Auth::user()->hak_akses == 2)
    @include('template.alert')
    <div class="card">
      <form action="{{ route('alterpart.store') }}" method="post">
        @csrf
        <div class="card-header">
          <h4>Part Data <a href="{{ route('barang.create') }}" class="btn btn-info ml-2"><i class="fas fa-arrow-left"></i> Back</a></h4>
        </div>
        <div class="card-body">
          <div class="form-group row">
            <label class="col-12 col-md-2 col-form-label text-md-right">Supplier</label>
            <div class="col-sm-12 col-md-8">
            <input type="text" class="form-control" name="supplier" readonly value="{{ Auth::user()->name }}">
            </div>
          </div>

          <div class="form-group row">
            <label class="col-12 col-md-2 col-form-label text-md-right">Kode Part Request</label>
            <div class="col-sm-12 col-md-8">
                <select name="kode_part" id="kode_part" class="form-control select2" required="" onchange="check()">
                    <option value=""></option>
                    @foreach ($datas as $e)
                    <option value="{{$e->kode_part}}">{{$e->kode_part}}</option>
                    @endforeach
                </select>
            </div>
            </div>
            <hr style="background-color: skyblue;">
            <div class="form-group row">
                <label class="col-12 col-md-2 col-form-label text-md-right">Kode Part Alternative</label>
                <div class="col-sm-12 col-md-8" id="alter">
                    <input type="text" class="form-control" name="kode_alterpart" required="">
                </div>
            </div>
          {{-- <div class="form-group row">
                <label class="col-12 col-md-2 col-form-label text-md-right">Kode Part</label>
                <div class="input-group mb-3 col-sm-12 col-md-8" id="kode">
                    <div class="input-group-prepend">
                      <button class="btn btn-outline-secondary dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Pilih Kode Part</button>
                      <div class="dropdown-menu">
                        <select name="kode_part" id="kode_part" class="form-control select2" onchange="check()">
                            <option value="">-Part-</option>
                            @foreach ($datas as $data)
                            <option class="dropdown-item" value="{{$data->kode_part}}">{{$data->kode_part}}</option>
                            @endforeach
                        </select>
                      </div>
                    </div>
                    <input type="text" class="form-control" name="kode_part" required="">
                </div>
              </div> --}}

              <div class="form-group row">
                <label class="col-12 col-md-2 col-form-label text-md-right">Nama Part</label>
                <div class="col-sm-12 col-md-8" id="part">
                    <input type="text" class="form-control" name="nama_part" required="">
                </div>
              </div>

              <div class="form-group row">
                <label class="col-12 col-md-2 col-form-label text-md-right">Brand Part</label>
                <div class="col-sm-12 col-md-8" id="brand">
                    <input type="text" class="form-control" name="brand" required="">
                </div>
              </div>

              <div class="form-group row">
                    <label class="col-12 col-md-2 col-form-label text-md-right">Merek Mobil</label>
                    <div class="col-sm-12 col-md-8">
                        <select name="merek" id="merek" class="form-control select2" required="" onchange="ada()">
                            <option value=""></option>
                            @foreach ($mobil as $e)
                            <option value="{{$e->merek}}">{{$e->merek}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group row">
                        <label class="col-12 col-md-2 col-form-label text-md-right">Model Mobil</label>
                        <div class="col-sm-12 col-md-8">
                            <select name="model" id="model" class="form-control select2" required="">
                                <option value=""></option>
                                @foreach ($model as $e)
                                <option value="{{$e->model}}">{{$e->model}}</option>
                                @endforeach
                            </select>
                        </div>
                </div>

                <div class="form-group row">
                        <label class="col-12 col-md-2 col-form-label text-md-right">Tahun</label>
                        <div class="col-sm-12 col-md-8" id="tahun">
                            <input type="number" class="form-control" placeholder="YYYY" min="1950" max="2020" maxlength="4" name="tahun" required="">
                        </div>
                  </div>

          <div class="form-group row">
            <label class="col-12 col-md-2 col-form-label text-md-right">Quantity</label>
            <div class="col-sm-12 col-md-8">
                <input type="text" class="form-control" name="quantity" required="">
                {{-- @php
                    $tanggal = date("Y-m-d");
                @endphp
                <input type="hidden" class="form-control" name="tanggal" required="" value="{{ $tanggal }}">
                <input type="hidden" class="form-control" name="status" required value="1"> --}}
            </div>
          </div>

          {{-- <div class="form-group row">
            <label class="col-12 col-md-2 col-form-label text-md-right">UoM</label>
            <div class="col-sm-12 col-md-8">
                <select name="uom" class="form-control" required="">
                    <option value="">Pilih UoM</option>
                    <option value="PCS">PCS</option>
                    <option value="Liter">Liter</option>
                    <option value="Box">Box</option>
                    <option value="Drum">Drum</option>
                    <option value="Galon">Galon</option>
                    <option value="Lusin">Lusin</option>
                    <option value="Kg">Kg</option>
                  </select>
            </div>
          </div> --}}

          <small id="formatUang" style="text-align:center;" class="form-text text-muted"></small>
          <div class="form-group row">
            <label class="col-12 col-md-2 col-form-label text-md-right">Harga</label>
            <div class="col-sm-12 col-md-8">
                <div class="input-group-prepend">
                    <span class="input-group-text">Rp.</span>
                    <input type="number" class="form-control" name="harga" required="" onkeyup="formatRupiah(this.value,'formatUang')">
                </div>
            </div>
          </div>
        </div>
        <div class="card-footer text-right">
            <a href="{{ route('home') }}" class="btn btn-danger ml-2">Cancel</a>
            <button class="btn btn-success">Submit</button>
        </div>
      </form>
    </div>
@endif
  </div>
  <script>
        function formatRupiah(angka, target) {
            var number_string = angka.replace(/[^,\d]/g, '').toString(),
                split = number_string.split(','),
                sisa = split[0].length % 3,
                rupiah = split[0].substr(0, sisa),
                ribuan = split[0].substr(sisa).match(/\d{3}/gi);

            // tambahkan titik jika yang di input sudah menjadi angka ribuan
            if (ribuan) {
                separator = sisa ? '.' : '';
                rupiah += separator + ribuan.join('.');
            }

            rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
            document.getElementById(target).innerHTML = 'Rp. ' + rupiah;
        }
    </script>
    <script>
        function check() {
        //var count = $('#kode_part option:selected').length;
        var cek = $('#kode_part').val();
        var selectPart = $('#part');
        var selectKode = $('#kode');
        var selectMerek = $('#merek');
        var selectMobil = $('#model');
        var selectTahun = $('#tahun');

        $.ajax({
            url: "{{ url('getpart')}}" + "/" + cek,
            type: "GET",
            dataType: "json",
            success: function(data) {
                //console.log(data)
                selectPart.empty()
                selectMerek.empty()
                selectMobil.empty()
                selectTahun.empty()
                selectKode.empty()
                1 // $('select[name="id_Mobil"]').empty('');
                // selectMobil.append('<option value="">--Pilih Model--</option>');
                $.each(data, function(key, value) {
                    selectKode.append('<input class="form-control" name="kode_part" readonly value="' + value.kode_part + '">');
                    selectPart.append('<input class="form-control" name="nama_part" value="' + value.nama_part + '">');
                    selectMerek.append('<option value="' + value.merek + '">' + value.merek + '</option>');
                    selectMobil.append('<option value="' + value.model + '">' + value.model + '</option>');
                    selectTahun.append('<input class="form-control" name="tahun" min="1950" max="2020" maxlength="4" value="' + value.tahun + '">');
                    // console.log(value.model)
                    // $('select[name="id_kota"]').attr('disabled',false);
                });
            }
        });
    }

    function ada() {
          var cek = $('#merek').val();
        //   console.log(cek)
          var selectMobil = $('#model');

          $.ajax({
            url: "{{ url('getmerek')}}" + "/" + cek,
            type: "GET",
            dataType: "json",
            success: function(data) {
                // console.log(data)
                selectMobil.empty()
                1 // $('select[name="id_Mobil"]').empty('');
                selectMobil.append('<option value=""></option>');
                $.each(data, function(key, value) {
                    selectMobil.append('<option value="' + value.model + '">' + value.model + '</option>');
                    // $('select[name="id_kota"]').attr('disabled',false);
                });
            }
        });
    }
    </script>
@endsection