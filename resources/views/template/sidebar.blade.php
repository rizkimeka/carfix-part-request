@php
  $route = Route::currentRouteName();
@endphp

<div class="main-sidebar sidebar-style-2">
  <aside id="sidebar-wrapper">
    <div class="sidebar-brand">
      <a href="/home">Carfix Part Request</a>
    </div>
    <div class="sidebar-brand sidebar-brand-sm">
      <a href="/home">CPR</a>
    </div>
      <ul class="sidebar-menu">
            <li class="menu-header">DASHBOARD</li>
            <li class=""><a class="nav-link" href="/home">
              <i class="fas fa-home"></i> <span>Dashboard</span></a>
            </li>
            @if(Auth::user()->hak_akses == 1)
            <li class="menu-header">PARTMAN</li>
            <li class="{{ $route == 'barang' || $route == 'barang.create' ? 'active' : '' }}"><a class="nav-link" href="{{ route('barang') }}">
                <i class="fas fa-handshake"></i> <span>Request Part</span></a>
            </li>
            @endif
            @if(Auth::user()->hak_akses == 2)
            <li class="menu-header">SUPPLIER</li>
            <li class="{{ $route == 'barang' || $route == 'barang.create' ? 'active' : '' }}"><a class="nav-link" href="{{ route('barang') }}">
                <i class="fas fa-edit"></i> <span>Input Part</span></a>
            </li>
            <li class="{{ $route == 'supplier' || $route == 'supplier.create' ? 'active' : '' }}"><a class="nav-link" href="{{ route('supplier') }}">
                <i class="fas fa-cart-plus"></i> <span>Data Supplier</span></a>
            </li>
            @endif
      </ul>
    <!-- <div class="mt-4 mb-4 p-3 hide-sidebar-mini">
      <a href="#" class="btn bg-white btn-lg btn-block btn-icon-split">
        <i class="fas fa-rocket"></i> Contact Developer
      </a>
    </div>         -->
  </aside>
</div>
