@extends('template.main')

@section('title','Add Supplier')

@section('content')
<div class="section-body">
    <h2 class="section-title">Input Supplier</h2>
    <p class="section-lead">Halaman untuk input supplier</p>
    @include('template.alert')
    <div class="card">
      <form action="{{ route('supplier.store') }}" method="post">
        @csrf
        <div class="card-header">
          <h4>Supplier Data <a href="{{ route('supplier') }}" class="btn btn-info ml-2"><i class="fas fa-arrow-left"></i> Back</a></h4>
        </div>
        <div class="card-body">
          <div class="form-group row">
            <label class="col-12 col-md-2 col-form-label text-md-right">Kode Supplier</label>
            <div class="col-sm-12 col-md-8">
              <input type="text" class="form-control" name="kode_supplier" required="">
            </div>
          </div>

          <div class="form-group row">
            <label class="col-12 col-md-2 col-form-label text-md-right">Nama Supplier</label>
            <div class="col-sm-12 col-md-8">
              <input type="text" class="form-control" name="nama_supplier" required="">
            </div>
          </div>

          <div class="form-group row">
            <label class="col-12 col-md-2 col-form-label text-md-right">No Telepon</label>
            <div class="col-sm-12 col-md-8">
                <input type="number" class="form-control" name="no_telp" required="">
            </div>
          </div>

          <div class="form-group row">
            <label class="col-12 col-md-2 col-form-label text-md-right">No HP</label>
            <div class="col-sm-12 col-md-8">
                <input type="number" class="form-control" name="no_hp" required="">
            </div>
          </div>

          <div class="form-group row">
            <label class="col-12 col-md-2 col-form-label text-md-right">Alamat</label>
            <div class="col-sm-12 col-md-8">
                <textarea class="form-control" name="alamat" required=""></textarea>
            </div>
          </div>

          <div class="form-group row">
            <label class="col-12 col-md-2 col-form-label text-md-right">Kecamatan</label>
            <div class="col-sm-12 col-md-8">
                <input type="text" class="form-control" name="kecamatan" required="">
            </div>
          </div>

          <div class="form-group row">
            <label class="col-12 col-md-2 col-form-label text-md-right">Kota</label>
            <div class="col-sm-12 col-md-8">
                <input type="text" class="form-control" name="kota" required="">
            </div>
          </div>

          <div class="form-group row">
            <label class="col-12 col-md-2 col-form-label text-md-right">Provinsi</label>
            <div class="col-sm-12 col-md-8">
                <input type="text" class="form-control" name="provinsi" required="">
            </div>
          </div>

          <div class="form-group row">
            <label class="col-12 col-md-2 col-form-label text-md-right">Kode Pos</label>
            <div class="col-sm-12 col-md-8">
                <input type="number" id="quantity" maxlength="5" class="form-control" name="kode_pos" required="">
            </div>
          </div>

        </div>
        <div class="card-footer text-right">
          <button class="btn btn-primary">Submit</button>
        </div>
      </form>
    </div>
  </div>

  <script>
      var inputQuantity = [];
        $(function() {
            $("#quantity").each(function(i) {
                inputQuantity[i] = this.defaultValue;
                $(this).data("idx", i); // save this field's index to access later
            });
            $("#quantity").on("keyup", function(e) {
                var $field = $(this),
                    val = this.value,
                    $thisIndex = parseInt($field.data("idx"), 10); // retrieve the index
                //        window.console && console.log($field.is(":invalid"));
                //  $field.is(":invalid") is for Safari, it must be the last to not error in IE8
                if (this.validity && this.validity.badInput || isNaN(val) || $field.is(":invalid")) {
                    this.value = inputQuantity[$thisIndex];
                    return;
                }
                if (val.length > Number($field.attr("maxlength"))) {
                    val = val.slice(0, 5);
                    $field.val(val);
                }
                inputQuantity[$thisIndex] = val;
            });
        });
  </script>
@endsection
