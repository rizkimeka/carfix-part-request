@extends('template.main')

@section('title','Form Supplier')

@section('content')
<div class="section-body">
    <h2 class="section-title">Supplier</h2>
    <p class="section-lead">Halaman index supplier</p>
    @include('template.alert')
    <div class="card shadow">
      <div class="card-header">
        <h4>Data Table</h4>
        <div class="card-header-action">
          <a href="{{ route('supplier.create') }}" class="btn btn-info">Add Data <i class="fas fa-plus"></i></a>
        </div>
      </div>
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-bordered table-striped table-hover" id="example">
            <thead>
              <tr>
                <th><i class="fas fa-th"></i></th>
                <th>Kode Supplier</th>
                <th>Nama Supplier</th>
                <th>No Telp</th>
                <th>No HP</th>
                <th>Alamat</th>
                <th>Kecamatan</th>
                <th>Kota</th>
                <th>Provinsi</th>
                <th>Kode Pos</th>
                <th width='20%'>Action</th>
              </tr>
            </thead>
            <tbody>
            @if(count($data) > 0)
                @foreach($data as $field)
                <tr>
                  <td>{{ $loop->iteration }}</td>
                  <td>{{ $field->kode_supplier }}</td>
                  <td>{{ $field->nama_supplier }}</td>
                  <td>{{ $field->no_telp }}</td>
                  <td>{{ $field->no_hp }}</td>
                  <td>{{ $field->alamat }}</td>
                  <td>{{ $field->kecamatan }}</td>
                  <td>{{ $field->kota }}</td>
                  <td>{{ $field->provinsi }}</td>
                  <td>{{ $field->kode_pos }}</td>
                  <td nowrap="">
                    <a href="{{ route('supplier.edit', [$field->kode_supplier]) }}" class="btn btn-icon btn-primary"><i class="fas fa-pen"></i></a>
                    <a onclick="return confirm('Apa anda yakin?')" href="{{ route('supplier.delete', [$field->kode_supplier]) }}" class="btn btn-danger"><i class="menu-icon fa fa-trash"></i>
                    </a>
                  </td>
                </tr>
                @endforeach
              @else
                <tr class="text-center">
                  <td colspan="4">No data found</td>
                </tr>
              @endif
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
@endsection
