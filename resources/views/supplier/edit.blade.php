@extends('template.main')

@section('title','Edit Supplier')

@section('content')
<div class="section-body">
    <h2 class="section-title">Edit Supplier</h2>
    <p class="section-lead">Halaman untuk edit supplier</p>
    @include('template.alert')
    <div class="card">
        <form action="{{ route('supplier.update',$data['kode_supplier']) }}" method="post">
            @csrf @method('patch')
        <div class="card-header">
          <h4>Supplier Data <a href="{{ route('supplier') }}" class="btn btn-info ml-2"><i class="fas fa-arrow-left"></i> Back</a></h4>
        </div>
        <div class="card-body">
          <div class="form-group row">
            <label class="col-12 col-md-2 col-form-label text-md-right">Kode Supplier</label>
            <div class="col-sm-12 col-md-8">
            <input type="text" class="form-control" name="kode_supplier" disabled="" value="{{ $data->kode_supplier}}">
            </div>
          </div>

          <div class="form-group row">
            <label class="col-12 col-md-2 col-form-label text-md-right">Nama Supplier</label>
            <div class="col-sm-12 col-md-8">
              <input type="text" class="form-control" name="nama_supplier" required="" value="{{ $data->nama_supplier}}">
            </div>
          </div>

          <div class="form-group row">
            <label class="col-12 col-md-2 col-form-label text-md-right">No Telepon</label>
            <div class="col-sm-12 col-md-8">
                <input type="number" class="form-control" name="no_telp" value="{{ $data->no_telp}}">
            </div>
          </div>

          <div class="form-group row">
            <label class="col-12 col-md-2 col-form-label text-md-right">No HP</label>
            <div class="col-sm-12 col-md-8">
                <input type="number" class="form-control" name="no_hp" required="" value="{{ $data->no_hp}}">
            </div>
          </div>

          <div class="form-group row">
            <label class="col-12 col-md-2 col-form-label text-md-right">Alamat</label>
            <div class="col-sm-12 col-md-8">
                <textarea type="text" class="form-control" name="alamat" required="">{{ $data->alamat}}</textarea>
            </div>
          </div>

          <div class="form-group row">
            <label class="col-12 col-md-2 col-form-label text-md-right">Kecamatan</label>
            <div class="col-sm-12 col-md-8">
                <input type="text" class="form-control" name="kecamatan" required="" value="{{ $data->kecamatan}}">
            </div>
          </div>

          <div class="form-group row">
            <label class="col-12 col-md-2 col-form-label text-md-right">Kota</label>
            <div class="col-sm-12 col-md-8">
                <input type="text" class="form-control" name="kota" required="" value="{{ $data->kota}}">
            </div>
          </div>

          <div class="form-group row">
            <label class="col-12 col-md-2 col-form-label text-md-right">Provinsi</label>
            <div class="col-sm-12 col-md-8">
                <input type="text" class="form-control" name="provinsi" required="" value="{{ $data->provinsi}}">
            </div>
          </div>

          <div class="form-group row">
            <label class="col-12 col-md-2 col-form-label text-md-right">Kode Pos</label>
            <div class="col-sm-12 col-md-8">
                <input type="number" class="form-control" name="kode_pos" required="" value="{{ $data->kode_pos}}">
            </div>
          </div>

        </div>
        <div class="card-footer text-right">
          <button class="btn btn-primary">Submit</button>
        </div>
      </form>
    </div>
  </div>
@endsection
