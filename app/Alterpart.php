<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Alterpart extends Model
{
    protected $table = 'tb_alterpart';
    public $timestamps = false;

    protected $guarded = [];

    public function part()
    {
        return $this->hasOne('App\Barang', 'kode_part', 'kode_part');
    }
}
