<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Supplier extends Model
{
    protected $table = 'tb_supplier';
    protected $primarykey = 'kode_supplier';
    public $timestamps = false;

    protected $guarded = [];
}
