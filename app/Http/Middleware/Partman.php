<?php

namespace App\Http\Middleware;

use Closure;

class Partman
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!auth()->guard('web')->check()) {
            return redirect('/login');
        }

        if (isset(auth()->user()->hak_akses)) {
            if (auth()->user()->hak_akses == 2) {
                return redirect()->route('access.denied');
            }
        }
        return $next($request);
    }
}
