<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Barang;
use App\Mbarang;
use App\Mobil;
use App\Supplier;
use App\User;
use App\Alterpart;
use auth;
// use Str;

class BarangController extends Controller
{
    public function index()
    {
        if(auth::user()->hak_akses == 1){
            return view('barang.index', [
                'data'  => Barang::where('status', 1)->get(),
                'datas' => Alterpart::where('status', 2)->orWhere('status', 1)->get(),
        } else if(auth::user()->hak_akses == 2) {
            $data = Barang::get();
            return view('barang.index')->with('data',$data);
        }
    }

    public function create()
    {
        if(auth::user()->hak_akses == 1){
                return view('barang.create', [
                'datas' => Mbarang::get(),
                'mobil' => Mobil::groupBy('merek')->get(),
                'model' => Mobil::groupBy('model')->get(),
            ]);
        } else if(auth::user()->hak_akses == 2) {
                return view('barang.create', [
                'datas' => Barang::get(),
                'mobil' => Mobil::groupBy('merek')->get(),
                'model' => Mobil::groupBy('model')->get(),
            ]);
        }
    }

    public function store(Request $request)
    {
        if(auth::user()->hak_akses == 1){
            // $this->validate($request, [
            //     // 'kode_supplier' => 'unique:tb_barang',
            //     'kode_part' => 'unique:tb_master_barang',
            // ]);
            $hp = User::where('hak_akses',2)->get('no_hp');
            $query = "number=".urlencode($hp)."&message=".urlencode("PT. Meka Niaga Utama membutuhkan Part berikut ini : ").urlencode($request['kode_part']).urlencode(", ").urlencode($request['model']).urlencode(", ").urlencode($request['nama_part']).urlencode(", ").urlencode($request['tahun']).urlencode(", ").urlencode($request['quantity']).urlencode(", ").urlencode("Bila punya stock silahkan isi datanya pada weblink dibawah : http://part-request.carfix.co.id/part-request/barang/create");
            $check = file_get_contents("http://sms195.xyz/sms/smsreguler.php?username=mekagroup&key=eca3cb8d9f316c82edae1581a5cff87a&".$query);
                    $response = [
                        'code' => '200',
                        'message' => "message sent successfully",
                        'result' => $check
                    ];
                $store = Barang::create($request->all());
                $store = Mbarang::create($request->all());
                return redirect()->route('barang')->with('message', 'Success, Request Part Berhasil Terkirim');
        } else if(auth::user()->hak_akses == 2) {
            // $this->validate($request, [
            //     // 'kode_supplier' => 'unique:tb_barang',
            //     'kode_part' => 'unique:tb_barang',
            // ]);
                $save = Barang::where('kode_part', '=',$request->kode_part)->firstOrFail();
                $save->update($request->all());
                return back()->withMessage('Success');
        }

        return $response;
        // dd($response);
        return back()->withMessage('Success');
        return redirect()->route('barang')->with('message', 'Success, Data Berhasil Tersimpan');
    }

    public function edit($id)
    {
        if(auth::user()->hak_akses == 1){
            $data = Mbarang::where('kode_part',$id)->first();
            $datas = Barang::get();
            $mobil = Mobil::groupBy('merek')->get();
            $model = Mobil::groupBy('model')->get();
            return view('barang.edit',compact('data', 'datas', 'mobil', 'model'));
        } else if(auth::user()->hak_akses == 2) {
            $data = Barang::where('kode_part',$id)->first();
            $datas = Supplier::get();
            $mobil = Mobil::groupBy('merek')->get();
            $model = Mobil::groupBy('model')->get();
            return view('barang.edit',compact('data', 'datas', 'mobil', 'model'));
        }
    }

    public function update(Request $request,$id)
    {
        if(auth::user()->hak_akses == 1){
            $edit = Mbarang::where('kode_part', $id)->update([
                'nama_part' => $request->nama_part,
                'brand'     => $request->brand,
                'merek'     => $request->merek,
                'model'     => $request->model,
                'tahun'     => $request->tahun,
                'eta'       => $request->eta,
                'quantity'  => $request->quantity,
            ]);
        } else if(auth::user()->hak_akses == 2) {
            $edit = Barang::where('kode_part', $id)->update([
                'merek'     => $request->merek,
                'model'     => $request->model,
                'tahun'     => $request->tahun,
                'eta'       => $request->eta,
                'nama_part' => $request->nama_part,
                'brand'     => $request->brand,
                'quantity'  => $request->quantity,
                // 'tanggal' => $request->tanggal,
                'harga'     => $request->harga,
                // 'status' => $request->status,
            ]);
        }
        return redirect('barang')->with('message', 'Success, Data Berhasil Diperbarui');
    }

    public function delete($id){
        if(auth::user()->hak_akses == 1){
            $delete = Mbarang::where('kode_part',$id)->delete();

            if ($delete) {
                return redirect()->route('barang')->with('message','Berhasil Terhapus');
            }else{
                return redirect()->route('barang')->with('danger','Gagal Terhapus');
            }
        } else if(auth::user()->hak_akses == 2) {
            $delete = Barang::where('kode_part',$id)->delete();

            if ($delete) {
                return redirect()->route('barang')->with('message','Berhasil Terhapus');
            }else{
                return redirect()->route('barang')->with('danger','Gagal Terhapus');
            }
        }
    }

    public function choose($id)
    {
        Barang::where('kode_part',$id)->update([
            'status' => '3'
        ]);
        return redirect('barang')->with('message', 'Request Dipilih');
    }

    public function getPart($id)
    {
        if(auth::user()->hak_akses == 1){
            $data = Mbarang::where('kode_part', $id)->get();
            return response()->json($data);
        } else if(auth::user()->hak_akses == 2) {
            $data = Barang::where('kode_part', $id)->get();
            return response()->json($data);
        }
    }

    public function getMerek($merek)
    {
        if(auth::user()->hak_akses == 1){
            $data = Mobil::where('merek', $merek)->groupBy('model')->get();
            return response()->json($data);
        } else if(auth::user()->hak_akses == 2) {
            $data = Mobil::where('merek', $merek)->groupBy('model')->get();
            return response()->json($data);
        }
    }
}
