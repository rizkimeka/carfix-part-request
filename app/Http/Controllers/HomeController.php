<?php

namespace App\Http\Controllers;

use App\Alterpart;
use App\Barang;
use App\Mobil;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home', [
            'data'  => Barang::get(),
            'datas' => Alterpart::get(),
        ]);
    }

    public function create()
    {
        return view('alterpart', [
            'datas' => Barang::get(),
            'mobil' => Mobil::groupBy('merek')->get(),
            'model' => Mobil::groupBy('model')->get(),
        ]);
    }

    public function store(Request $request)
    {
        // $this->validate($request, [
        //     'kode_part' => 'unique:tb_alterpart',
        // ]);
        $save = Alterpart::create($request->all());
        return back()->withMessage('Success');
        return redirect()->route('alterpart')->with('message', 'Success, Data Berhasil Tersimpan');
    }

    public function delete($id){
        $delete = Alterpart::where('kode_part',$id)->delete();

            if ($delete) {
                return redirect()->route('barang')->with('message','Berhasil Terhapus');
            }else{
                return redirect()->route('barang')->with('danger','Gagal Terhapus');
            }
    }
    
    public function pilih($id)
    {
        Alterpart::where('kode_part',$id)->update([
            'status' => '3'
        ]);
        return redirect('barang')->with('message', 'Request Dipilih');
    }

    public function denied()
    {
        return view('accessdenied');
    }
}
