<?php

namespace App\Http\Controllers;
use App\Supplier;

use Illuminate\Http\Request;

class SupplierController extends Controller
{
    public function index()
    {
        $data = Supplier::get();
        return view('supplier.index')->with('data',$data);
    }

    public function create()
    {
        return view('supplier.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'kode_supplier' => 'unique:tb_supplier',
        ]);
        $save = Supplier::create($request->all());
        return back()->withMessage('Success');
        return redirect()->route('supplier')->with('message', 'Success, Data Berhasil Tersimpan');
    }

    public function edit($id)
    {
        $data = Supplier::where('kode_supplier',$id)->first();
        return view('supplier.edit')->with('data', $data);
    }

    public function update(Request $request,$id)
    {
        $edit = Supplier::where('kode_supplier', $id)->update([
            'nama_supplier' => $request->nama_supplier,
            'no_telp' => $request->no_telp,
            'no_hp' => $request->no_hp,
            'alamat' => $request->alamat,
            'kecamatan' => $request->kecamatan,
            'kota' => $request->kota,
            'provinsi' => $request->provinsi,
            'kode_pos' => $request->kode_pos,
        ]);
        return redirect('supplier')->with('message', 'Success, Data Berhasil Diperbarui');
    }

    public function delete($id){
        $delete = Supplier::where('kode_supplier',$id)->delete();

        if ($delete) {
            return redirect()->route('supplier')->with('message','Berhasil Terhapus');
        }else{
            return redirect()->route('supplier')->with('danger','Gagal Terhapus');
        }
    }
}
