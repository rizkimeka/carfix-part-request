<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cabang extends Model
{
    protected $table = 'tb_cabang';
    public $timestamps = false;

    protected $guarded = [];

    public function user()
    {
        return $this->hasOne('App\User', 'cabang', 'nama_cabang');
    }
}
