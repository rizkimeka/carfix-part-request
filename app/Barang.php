<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Barang extends Model
{
    protected $table = 'tb_barang';
    public $timestamps = false;

    protected $guarded = [];

    public function user()
    {
        return $this->hasOne('App\User', 'name', 'supplier');
    }

    public function mbarang()
    {
        return $this->hasOne('App\Mbarang', 'kode_part', 'kode_part');
    }
}
